import { SharedService } from '../../services/shared.service';
import { Component, Injectable, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
@Injectable({
  providedIn: 'root'
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;
  selectedCard = JSON.parse(localStorage.getItem("selectedCard")) ?
              JSON.parse(localStorage.getItem("selectedCard")).length : 0;

  constructor(private ss: SharedService) {

  }
  ngOnInit(): void {
    this.ss.getSelectedCardCount().subscribe((count: number) => {
      this.selectedCard = count
    });
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
