import { StorageProduct } from './../../types/product';
import { SharedService } from '../../services/shared.service';
import { ProductService } from '../../services/product.service';
import { Component, OnInit, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/product';
import { switchMap } from 'rxjs/operators';

@Component({
    selector: 'catalog',
    templateUrl: './catalog.component.html',
    styleUrls: ['./catalog.component.css']
})
@Injectable({
    providedIn: 'root'
})
export class CatalogComponent implements OnInit {

    public products: Product[];
    public filteredProduct: Product[];
    public isLoading = true;

    constructor(private route: ActivatedRoute, private ProductService: ProductService, private ss: SharedService) { }

    ngOnInit(): void {
        this.getAllProducts();
    }

    getAllProducts() {
        this.isLoading = true;
        this.ProductService.getAllProducts().pipe(switchMap((data: Product[]) => {
                this.filteredProduct = data;
                return this.route.queryParams;
            }
        )).subscribe(params => {
            this.isLoading = false;
            this.products = this.filteredProduct;
        })
    }
    
    public addBook(id: number) {
        let selectedCards: StorageProduct[] = JSON.parse(localStorage.getItem("selectedCard")) || [];
        const selectedProductIndex = this.products.findIndex(item => item.id === id);
        const selectedProduct = this.products[selectedProductIndex];
        if(selectedProduct.stock > 0) {
            let newProductCard: StorageProduct = {
                id: id,
                count: 1
            }
            if (selectedCards.length == 0) {
                selectedCards.push(newProductCard);
            } else {
                const index = selectedCards.findIndex(item => item.id === id);
                if(index == -1) {
                    selectedCards.push(newProductCard);
                }
            }
        }
        localStorage.setItem("selectedCard", JSON.stringify(selectedCards));
        this.ss.setSelectedCardCount(selectedCards.length);
    }
}