import { ChangeItemsAction } from './../../enums/genericEnums';
import { StorageProduct } from './../../types/product';
import { SelectedProduct } from './../../models/product';
import { SharedService } from './../../services/shared.service';
import { switchMap } from 'rxjs/operators';
import { ProductService } from './../../services/product.service';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/product';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'selected-cars',
    templateUrl: './selectedCards.component.html',
    styleUrls: ['./selectedCards.component.css']
})

export class SelectedCardsComponent implements OnInit {
    public selectedProducts: SelectedProduct[];
    public total: number;
    public isLoading = true;
    public productsWereChecked = false;
    private currentIds = []

    constructor(private route: ActivatedRoute, private ProductService: ProductService, private ss: SharedService) { }

    ngOnInit(): void {
        this.productsWereChecked = false;
        let selectedCard: StorageProduct[] = JSON.parse(localStorage.getItem("selectedCard"));
        let ids: number[] = [];
        for(let i = 0; i < selectedCard.length; i++) {
            ids.push(selectedCard[i].id);
        }
        this.getSelectedProducts(ids);
    }

    public calculateTotal() {
        this.total = 0;
        for (let i = 0; i < this.selectedProducts.length; i++) {
            this.total += this.selectedProducts[i].price * this.selectedProducts[i].selectedCount;         
        }
        this.total = parseFloat(this.total.toFixed(2));
    }

    getSelectedProducts(ids: number[]) {
        this.currentIds = ids;
        this.isLoading = true;
        this.ProductService.getSelectedProducts(ids).pipe(switchMap((data: Product[]) => {
                let selectedCards: StorageProduct[] = JSON.parse(localStorage.getItem("selectedCard"));
                let selectedProducts: SelectedProduct[] = [];
                for(let i = 0; i < data.length; i++) {
                    let selectedCard = selectedCards.find(item => item.id == data[i].id)
                    let selectedProduct: SelectedProduct = {
                        id: data[i].id,
                        name: data[i].name,
                        description: data[i].description,
                        price: data[i].price,
                        stock: data[i].stock,
                        selectedCount: selectedCard.count
                    }
                    selectedProducts.push(selectedProduct);
                }
                this.selectedProducts = selectedProducts;
                return this.route.queryParams;
            }
        )).subscribe(params => {
            this.isLoading = false;
            this.calculateTotal();
        })
    }

    public removeFromCard(id: number) {
        let selectedCards: StorageProduct[] = JSON.parse(localStorage.getItem("selectedCard"));
        const filteredCards = selectedCards.filter(item => item.id != id);
        localStorage.setItem("selectedCard", JSON.stringify(filteredCards));
        let ids: number[] = [];
        for(let i = 0; i < filteredCards.length; i++) {
            ids.push(filteredCards[i].id);
        }
        this.getSelectedProducts(ids);
        this.ss.setSelectedCardCount(ids.length);
        this.calculateTotal();
    }

    public addItem(id: number, selectedCount: number, stock: number) {
        this.changeItemCount(ChangeItemsAction.Add, id, selectedCount, stock);
    }

    public removeItem(id: number, selectedCount: number, stock: number) {
        this.changeItemCount(ChangeItemsAction.Remove, id, selectedCount, stock);
    }

    private changeItemCount(action: number, id: number, selectedCount: number, stock: number) {
        let selectedCards: StorageProduct[] = JSON.parse(localStorage.getItem("selectedCard")) || [];
        const index = selectedCards.findIndex(item => item.id === id);
        const newCount = action === ChangeItemsAction.Add ? selectedCount + 1 : selectedCount - 1;
        if(index != -1 && newCount > 0 && newCount <= stock) {
            let newProductCard: StorageProduct = {
                id: id,
                count: newCount
            }
            selectedCards[index] = newProductCard;
        }
        localStorage.setItem("selectedCard", JSON.stringify(selectedCards));
        this.getSelectedProducts(this.currentIds);
        this.calculateTotal();
    }

    public checkOut() {
        let selectedCards: StorageProduct[] = JSON.parse(localStorage.getItem("selectedCard")) || [];
        if(selectedCards.length > 0) {
            selectedCards.forEach(e => {
                this.ProductService.checkOutProduct(e.id, e.count).pipe().subscribe(params => {})
            localStorage.setItem("selectedCard", JSON.stringify([]));
            })
        }
        this.ss.setSelectedCardCount(0)
        this.getSelectedProducts([]);
        this.productsWereChecked = true;
    }
}