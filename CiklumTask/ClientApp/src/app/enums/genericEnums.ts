export enum ChangeItemsAction {
    Add = 1,
    Remove = 2
}