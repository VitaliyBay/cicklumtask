import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({providedIn: 'root'})
export class SharedService {
    @Output() selectedCardCount: EventEmitter<number> = new EventEmitter();

    constructor() { }

    setSelectedCardCount(count: number) {
        this.selectedCardCount.emit(count);
    }

    getSelectedCardCount() {
        return this.selectedCardCount;
    }
    
}