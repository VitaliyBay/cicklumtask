import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class ProductService {
    baseURL: string;

    constructor(private http: HttpClient) {
        this.baseURL = "/api/product/";
    }

    getAllProducts() {
        return this.http.get(this.baseURL).pipe(map(response => response));
    }

    getSelectedProducts(ids: number[]) {
        let requestIds: string = "";
        for(let i = 0; i < ids.length; i++) {
            requestIds += "ids=" + ids[i] + "&"
        }
        const editedRequestIds = requestIds.slice(0, -1);
        return this.http.get(this.baseURL + "GetById" + "?" + editedRequestIds).pipe(map(response => {
            return response
        }));
    }

    checkOutProduct(id: number, count: number) {
        return this.http.get(this.baseURL + "CheckOut/" + id + "/" + count + "/").pipe();
    }
    
}