export class Product {
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number; 
}

export class SelectedProduct {
    id: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    selectedCount: number;
}