export type StorageProduct = {
    id: number,
    count: number
}