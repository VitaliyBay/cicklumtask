﻿using Ciklum.SqlMainDb.CheckOutProduct;
using Ciklum.SqlMainDb.FindProducts;
using Ciklum.SqlMainDb.GetProduct;
using Ciklum.SqlMainDb.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiklumTask.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IEnumerable<Product> Get()
        {
           return GetProductService.GetProducts();
        }

        [HttpGet("{ids}")]
        [Route("GetById/")]
        public IEnumerable<Product> GetById([FromQuery] int[] ids)
        {
            return FindProductsService.FindProducts(ids);
        }

        [HttpGet]
        [Route("CheckOut/{productId}/{count}/")]
        public bool CheckOut(int productId, int count)
        {
            return CheckOutProductService.CheckOutProduct(productId, count);
        }
    }
}
