﻿using Ciklum.SqlMainDb.FindProducts;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace Ciklum.SqlMainDb.CheckOutProduct
{
    public class CheckOutProductService
    {
        public static bool CheckOutProduct(int id, int count)
        {
            SqlConnection conn = DbContext.openConnection();
            try
            {
                List<Models.Product> products = FindProductsService.FindProducts(new int[]{id});
                if(products.Count > 0)
                {
                    Models.Product product = products[0];
                    if (product.Stock >= count)
                    {
                        int newStock = product.Stock - count;
                        return CheckOutProductDataService.CheckOutProduct(id, newStock);
                    }
                }
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
                return false;
            }
            finally
            {
                DbContext.closeConnection(conn);
            }
            return true;
        }
    }
}
