﻿using System;
using System.Data.SqlClient;

namespace Ciklum.SqlMainDb.CheckOutProduct
{
    internal class CheckOutProductDataService
    {
        public static bool CheckOutProduct(int id, int stock)
        {
            SqlConnection conn = DbContext.openConnection();
            try
            {
                string[][] arguments = new string[2][];
                arguments[0] = new string[] { "@stock", stock.ToString()};
                arguments[1] = new string[] { "@id", id.ToString()};
                DbContext.executeNonQuery(conn, "UPDATE tblProducts SET Stock = @stock Where Id = @id", arguments);
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
                return false;
            }
            finally
            {
                DbContext.closeConnection(conn);
            }
            return true;
        }
    }
}
