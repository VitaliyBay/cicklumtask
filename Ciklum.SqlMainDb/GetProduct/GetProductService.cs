﻿using System.Collections.Generic;

namespace Ciklum.SqlMainDb.GetProduct
{
    public class GetProductService
    {
        public static List<Models.Product> GetProducts()
        {
            return GetProductDataService.GetProducts();
        }
    }
}
