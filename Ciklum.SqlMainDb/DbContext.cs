﻿using System.Data;
using System.Data.SqlClient;

namespace Ciklum.SqlMainDb
{
    internal class DbContext
    {
        public static SqlConnection openConnection()
        {
            SqlConnection conn = new SqlConnection("Data Source=DESKTOP-O2K81VH;Initial Catalog=Ciklum_MainDb;Integrated Security=True");
            conn.Open();
            return conn;
        }

        public static SqlDataReader executeQuery(SqlConnection conn, string query, string[][] arguments)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 60;
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            foreach (var arg in arguments)
            {
                cmd.Parameters.AddWithValue(arg[0], arg[1]);
            }
            SqlDataReader dr = cmd.ExecuteReader();
            return dr;
        }

        public static void executeNonQuery(SqlConnection conn, string query, string[][] arguments)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 60;
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            foreach (var arg in arguments)
            {
                cmd.Parameters.AddWithValue(arg[0], arg[1]);
            }
            cmd.ExecuteNonQuery();
        }

        public static void closeConnection(SqlConnection conn)
        {
            conn.Close();
        }
    }
}
