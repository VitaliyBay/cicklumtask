﻿using System.Collections.Generic;

namespace Ciklum.SqlMainDb.FindProducts
{
    public class FindProductsService
    {
        public static List<Models.Product> FindProducts(int[] ids)
        {
            string ids_str = string.Join(",", ids);
            return FindProductsDataService.FindProducts(ids_str);
        }
    }
}
