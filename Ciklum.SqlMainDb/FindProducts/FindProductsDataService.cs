﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Ciklum.SqlMainDb.FindProducts
{
    internal class FindProductsDataService
    {
        public static List<Models.Product> FindProducts(string ids)
        {
            List<Models.Product> products = new List<Models.Product>();
            SqlConnection conn = DbContext.openConnection();
            try
            {
                SqlDataReader dr = DbContext.executeQuery(conn, "SELECT * FROM tblProducts WHERE id in (" + ids + ") ORDER BY name", new string[0][]);
                while (dr.Read())
                {
                    products.Add(new Models.Product { Id = (int)dr[0], Name = (string)dr[1], Description = (string)dr[2], Price = (double)dr[3], Stock = (int)dr[4] });
                }
            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }
            finally
            {
                DbContext.closeConnection(conn);
            }
            return products;
        }
    }
}
